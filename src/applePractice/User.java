package applePractice;

import lombok.Data;

@Data
public class User {

    private String topic;

    private String host;

    private int retry_limit;

    private int multiplication_factor;

    private int max_retry_limit;

    public User(String topic, String host, int retry_limit, int multiplication_factor, int max_retry_limit) {
        this.topic = topic;
        this.host = host;
        this.retry_limit = retry_limit;
        this.multiplication_factor = multiplication_factor;
        this.max_retry_limit = max_retry_limit;
    }
}
