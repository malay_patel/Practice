package applePractice;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test{
    public static void main(String[] args) {

        String abc = "skybox";

        List<User> users = Arrays.asList(
                new User("Kafka", "lapp01", 2, 5, 10),
                new User("Delegation", "lapp02", 3, 5, 10),
                new User("SkyBox", "lapp03", 2, 5, 10)
        );

        User kafkaUser = users.stream().filter(x -> abc.equalsIgnoreCase(x.getTopic())).findAny().orElse(null);
        System.out.println(kafkaUser);

        User user1 = new  User("Kafka", "lapp01", 2, 5, 10);
        User user2 = new User("Delegation", "lapp02", 3, 5, 10);
        List<User> usersss = new ArrayList<>();
        usersss.add(user1);
        usersss.add(user2);
        for(User u: usersss) {
            System.out.println(u);
        }
        /*List<User> lists = new ArrayList<>();
        lists.add(0, user1);
        for(User u: lists) {
            System.out.println(u);
        }*/
        int a =5;
        int b=10;

        System.out.printf("a=%s b=%s",a,b);
        System.out.println();

        try {
            InetAddress address =  InetAddress.getLocalHost();
            System.out.println(address.getHostAddress());
            System.out.println(address.getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }
}
