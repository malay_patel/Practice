package coreJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class Collection {
	/*public static void main(String[] args) {
		HashSet<Person> set = new HashSet<>();
		
		Person p1 = new Person("Viru", 1);
		Person p2 = new Person("Viru", 2);
		Person p3 = new Person("Viru", 3);
		set.add(new Person("Viru", 23));
		set.add(new Person("Viru", 23));
		set.add(new Person("Viru", 23));
		set.add(new Person("Viru", 23));
		set.add(new Person("Viru", 23));
		set.add(new Person("Viru", 23));
		set.add(p1);
		set.add(p2);
		set.add(p3);

		for (Person c : set) {
			System.out.print(c.getName() + " ");
			System.out.println(c.getAge());
		}
		
		List<Integer> list = new ArrayList<Integer>();
		for(int i =0; i<10; i++) list.add(i);
		System.out.println(list);
		for (int i : list) {
			System.out.print(i+" ");
			
		}
		//traversing through forEach method of Iterable with anonymous class
				list.forEach(new Consumer<Integer>() {

					public void accept(Integer t) {
						System.out.println("forEach anonymous class Value::"+t);
					}

				});
				
				//traversing with Consumer interface implementation
				MyConsumer action = new MyConsumer();
				list.forEach(action);
				
			}

		}

		//Consumer implementation that can be reused
		class MyConsumer implements Consumer<Integer>{

			public void accept(Integer t) {
				System.out.println("Consumer impl Value::"+t);
			}*/


	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<>(2);
		map.put(1, "Malay");
		map.put(2, "Devanshi");
		map.put(3, "Mom");
		map.put(4, "Daddy");
		//map.put(3, "Lalu");
		System.out.println(map);
		System.out.println(map.get(3));
		//System.out.println(reverse(map));
		for(Entry<Integer, String> entry : map.entrySet()) {
			if(entry.getValue() == "Malay") {
				System.out.println(entry.getKey());
			}
			//else {System.out.println("Couldn't found it");}
		}
		
		Map<String,Integer> newMap = map.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue,Map.Entry::getKey));
		System.out.println(newMap);
		
		/*HashMap<String, Integer> newMap1 = new HashMap<>();
		for(Map.Entry<Integer, String> entry: map.entrySet()) {
			newMap1.put(entry.getValue(), entry.getKey());
		}
		System.out.println(newMap1);
	*/}
	//reverse the map method
	public static <K,V> HashMap<V,K> reverse(Map<K,V> map) {
	    HashMap<V,K> rev = new HashMap<V, K>();
	    for(Map.Entry<K,V> entry : map.entrySet())
	        rev.put(entry.getValue(), entry.getKey());
	    return rev;
	}
	
		
	}

