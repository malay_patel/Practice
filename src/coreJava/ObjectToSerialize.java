/**
 * 
 */
package coreJava;

import java.io.Serializable;

/**
 * @author malay
 *
 */
public class ObjectToSerialize implements Serializable{


    private static final long serialVersionUID = 7526472295622776147L;

    private String firstName;
    private String lastName;

    public ObjectToSerialize(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return getFirstName() + " " + getLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectToSerialize that = (ObjectToSerialize) o;
        return firstName.equals(that.firstName) && lastName.equals(that.lastName);
    }
	
}
