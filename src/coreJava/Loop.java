package coreJava;

public class Loop {
	public static void main(String[] args) {

		for (int i = 1; i < 7; i++) {
            for (int j = 1; j < i + 1; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
		
		for(int i=7; i>=0; i--) {
			for(int j=7; j>i; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
	}
}
