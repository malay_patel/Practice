/**
 * 
 */
package coreJava;

import java.util.AbstractMap.SimpleImmutableEntry;

/**
 * @author malay
 *
 */
public class SingletonExample {

	//Simple Singleton
/*	private static SingletonExample  instance = null;
	protected SingletonExample() {}
	public static SingletonExample getInstance() {
		if(instance == null) {
			instance = new SingletonExample();
		}
		return instance;
	}*/
	
	//Synchronized Singleton
	/*private static SingletonExample  instance = null;
	protected SingletonExample() {}
	public synchronized static SingletonExample getInstance() {
		if(instance == null) {
			
			instance = new SingletonExample();
		}
		return instance;
	}*/
	
	
	//Performnace Enhacement
/*	private static SingletonExample  instance = null;
	protected SingletonExample() {}
	public static SingletonExample getInstance() {
		if(instance == null) {
			synchronized (SingletonExample.class) {
				instance = new SingletonExample();
			}
			
		}
		return instance;
	}*/
	
	//Double-checked locking
	private static SingletonExample  instance = null;
	protected SingletonExample() {}
	public static SingletonExample getInstance() {
		if(instance == null) {
			synchronized (SingletonExample.class) {
				if(instance == null) {
				instance = new SingletonExample();
				}
			}
			
		}
		return instance;
	}
	
}
