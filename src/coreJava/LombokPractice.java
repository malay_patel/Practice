
package coreJava;

import lombok.Data;

/**
 * @author malay
 *
 */
@Data public class LombokPractice {
	
	private String a;
	private int b;
	private Boolean c;
	
	/*public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
	public Boolean getC() {
		return c;
	}
	public void setC(Boolean c) {
		this.c = c;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + b;
		result = prime * result + ((c == null) ? 0 : c.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LombokPractice other = (LombokPractice) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (b != other.b)
			return false;
		if (c == null) {
			if (other.c != null)
				return false;
		} else if (!c.equals(other.c))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "LombokPractice [a=" + a + ", b=" + b + ", c=" + c + "]";
	}
	*//**
	 * @param a
	 * @param b
	 * @param c
	 *//*
	public LombokPractice(String a, int b, Boolean c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}*/

}
