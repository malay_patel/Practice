/**
 * 
 */
package coreJava;

/**
 * @author malay
 *
 */
public class DynamicInstantiation {
	  @SuppressWarnings("deprecation")
	public Object createInstance(String className) throws Exception {
	    Class c = Class.forName(className);
	    return c.newInstance();
	  }
}
