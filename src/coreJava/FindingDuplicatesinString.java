/**
 * 
 */
package coreJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author malay
 *
 */
public class FindingDuplicatesinString {

	public static void main(String[] args) {

		/*
		 * String[] names = {"java", "JavaScript", "java", "JavaScript", "Abc", "Abc",
		 * "123", "123"}; int[] numbers = {2, 3, 5, 1, 2, 3 ,4, 5, 6,1}; Set<Integer>
		 * numbersSet = new HashSet<>(); Set<String> store = new HashSet<>();
		 * List<Integer> duplicatesNumbers = new ArrayList<Integer>(); List<String>
		 * duplicates = new ArrayList<String>();
		 * 
		 * for (int num : numbers) { if(numbersSet.add(num) == false) {
		 * System.out.println("Found duplicate number: "+ num);
		 * duplicatesNumbers.add(num); } } Collections.sort(duplicatesNumbers);
		 * System.out.println(duplicatesNumbers);
		 * 
		 * for (String name : names) { if(store.add(name) == false) {
		 * System.out.println("Found Duplicate "+ name); duplicates.add(name); } }
		 * //sorting list of duplicates Collections.sort(duplicates);
		 * System.out.println(duplicates);
		 */

		String[] names = { "Malay", "Malay", "Java", "java", "java" };
		Set nameSet = new HashSet<String>();
		List nameList = new ArrayList<String>();

		for (String n : names) {
			if (nameSet.add(n) == false) {
				nameList.add(n);
			}
		}
		System.out.println(nameList);

	}

}
