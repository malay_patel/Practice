/**
 * 
 */
package coreJava;

import java.util.Scanner;

/**
 * @author malay
 *
 */
public class SticklerThief {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t=sc.nextInt();
		for (int i=0; i<t; i++) {
			int n=sc.nextInt();
			int arr[] = new int[n];
			for(int j=0; j<n; j++) 
				arr[j] = sc.nextInt();
			System.out.println(maxSum(n, arr));
			
		}
		
	}
	
	public static int maxSum(int n, int arr[]) {
		
		if(n==1)
			return arr[0];
		int dp[] = new int[n];
		dp[0] = arr[0];
		dp[1] = Math.max(arr[0], arr[1]);
		for(int i=2; i<n; i++) 
			dp[i] = Math.max(dp[i-2] + arr[i], dp[i-1]);

		return dp[n-1];
	}
}
