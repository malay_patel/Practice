/**
 * 
 */
package coreJava;

import java.io.File;
import java.util.ArrayList;

/**
 * @author malay
 *
 */
public class GetFileNamesFromDirectory {
	
	public static void main(String[] args) {
		ArrayList<File> namesOfFiles = new ArrayList<File>();
		
		getFiles("/Users/malay/git/iBPM/",namesOfFiles);
	}

	/**
	 * @param string
	 * @param namesOfFiles
	 */
	private static void getFiles(String filePath, ArrayList<File> namesOfFiles) {
		
		File file = new File(filePath);
		File[] listOfFiles = file.listFiles();
		
		for(File f: listOfFiles) {
			if(f.isFile()) {
				System.out.println("File Name: "+ f.getName());
				namesOfFiles.add(f);
			}
			if(f.isDirectory()) {
				System.out.println("Directory Name: "+ f.getName());
				getFiles(f.getAbsolutePath(), namesOfFiles);
			}
		}
		//System.out.println(namesOfFiles.size());
		
		
	}

}
