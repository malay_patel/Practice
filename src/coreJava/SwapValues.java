/**
 * 
 */
package coreJava;

/**
 * @author malay
 *
 */
public class SwapValues {

	//This swap method won't work because we are trying to change reference but after that we terminate the method
	//This is copy of reference so it is pass by value
	//Java is always pass by value
	public static void swap(int a, int b) { //a=50 , b=100 memory location
		int temp; 
		temp =a; // temp=50 , a=50, b=100
		a=b;		// temp=50, a=100, b=100
		b=temp;		// temp=50, a=100, b=50
		
					//after that method terminated
	}
	
	public static void main(String[] args) {
		int a=5;
		int b=10;

		swap(a, b); //method won't work because we changed reference of this object but method terminated so it doesn't change anything
		
		/*int temp;   	//this will work because we passing values here 
		temp =a;
		a=b;
		b=temp;*/
		
		/*a=a+b;
		b=a-b;
		a=a-b;*/
		
		/**
		 * In python
		 * 
		 * a,b = b,a
		 */
		
		System.out.println("A: " + a + " B: "+b);
	}
	
}
