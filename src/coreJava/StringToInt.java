/**
 * 
 */
package coreJava;

import java.util.Optional;

/**
 * @author malay
 *
 */
public class StringToInt {

	public static void main(String[] args) {
		String number = "10";
		String number1="10A";
		
		// 1st way
		int result = Integer.parseInt(number);
		System.out.println(result);
		
		// 2nd way
		Integer result1 = Integer.valueOf(number);
		System.out.println(result1);
		
		// 3rd way
		try {
			int result3 = Integer.parseInt(number1);
			System.out.println(result);
		}catch(NumberFormatException e){
			System.out.println(e);
		}
		
		//4th way
		Integer result4 = Optional.of(number).map(Integer::valueOf).get();
		System.out.println(result4);
	}
}
