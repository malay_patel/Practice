package coreJava;

import java.util.Arrays;

public class LongestCommonPrefix {

	public static String longestCommonPrefix(String[] strs) {
		
		Arrays.sort(strs);
	    if(strs==null || strs.length==0){
	        return "";
	    }
	 
	    if(strs.length==1) 
	        return strs[0];
	 
	    int minLen = strs.length+1;
	 
	    for(String str: strs){
	        if(minLen > str.length()){
	            minLen = str.length();
	        }
	    }
	 
	    for(int i=0; i<minLen; i++){
	        for(int j=0; j<strs.length-1; j++){
	            String s1 = strs[j];
	            String s2 = strs[j+1];
	            if(s1.charAt(i)!=s2.charAt(i)){
	                //return s1.substring(0, i);
	            		return s1;
	            }
	        }
	    }
	 
	    //return strs[0].substring(0, minLen);
	    return strs[0];
	}
	
	/*public static String longestCommonPrefix(String[] strings) {
	    if (strings.length == 0) {
	        return "";   // Or maybe return null?
	    }

	    for (int prefixLen = 0; prefixLen < strings[0].length(); prefixLen++) {
	        char c = strings[0].charAt(prefixLen);
	        for (int i = 1; i < strings.length; i++) {
	            if ( prefixLen >= strings[i].length() ||
	                 strings[i].charAt(prefixLen) != c ) {
	                // Mismatch found
	                //return strings[i].substring(0, prefixLen);
	            		return strings[i];
	            }
	        }
	    }
	    return strings[0];
	}
	*/
	public static void main(String[] args) {
		String[] names = {"Candle", "Car", "California", "Cancer"};
		//longestCommonPrefix(names);
		System.out.println(longestCommonPrefix(names));
	}
}
