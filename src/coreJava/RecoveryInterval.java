/**
 * Given a list of input tasks to run, and the cooldown interval, output the minimum number of time slots required to run them. 
// Tasks: 1, 1, 2, 1, 2 
// Recovery interval (cooldown): 2 
// Output: 8 (order is 1 _ _ 1 2 _ 1 2 ) 

========= 
Tasks are task numbers in that order coming in for execution. 
Cooling time is time interval required to cool down the machine after executing a task. 
So it's like if CPU executed task 1 then it needs 2 cooling time intervals before executing another task 1 but meanwhile,
 it can execute other tasks which are not same as 1 and so on. 
 So before executing any task, you have to check if you have executed same task number before and if yes, 
 then if its cooling time interval is done or not. 
The output is basically the number of cycles/time slots CPU took to execute these tasks in that order 
(including when task executed and cooling intervals).


 */
package coreJava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author malay
 *
 */
public class RecoveryInterval {

	public static int timeSlots(List<Character> tasks, int cooldown) {
	    if ( cooldown <= 0 ) { return tasks.size(); }

	    Map<Character, Integer> lastTimeSlot = new HashMap<>();
	    int result = 0;
	    int taskIndex = 0;

	    while ( taskIndex < tasks.size() ) {
	        char task = tasks.get(taskIndex);
	        Integer last = lastTimeSlot.get(task);
	        if ( last == null || result - last > cooldown ) {
	            lastTimeSlot.put(task, result);
	            taskIndex++;
	        }
	        result++;
	    }

	    return result;
	}
	public static void main(String[] args) {
		List<Character> tasks= new ArrayList<>();
		tasks.add('A');
		tasks.add('A');
		tasks.add('B');
		tasks.add('A');
		
		//tasks.add(2);
		
		System.out.println(timeSlots(tasks, 2));
	}
}
