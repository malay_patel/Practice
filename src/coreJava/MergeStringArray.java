/**
 * 
 */
package coreJava;

/**
 * @author malay
 *
 */
public class MergeStringArray {
	
	public static void main(String[] args) {
		String[] a = {"Dog", "Cat"};
		String[] b = {"Mal", "Nala"};
		String sA = "";
		String sB = "";
		
		for(String s: a) {
			sA+=s;
		}
		for(String s: b) {
			sB+=s;
		}
		
	
		MergeStringArray me = new MergeStringArray();
		System.out.println(me.merge(sA, sB));
	}

	public String merge(String leftStr, String rightStr) {  
        char[]left = leftStr.toCharArray();
        char[]right = rightStr.toCharArray();
        int nL = left.length;
        int nR= right.length;
        char[] mergeArr= new char[nL+nR];
        int i=0,j=0,k=0;
        int temp =0;
        while(i<nL && j<nR){

            if (temp==0){
                mergeArr[k]=left[i];
                i++;
                temp =1;
            }else{
                mergeArr[k]=right[j];
                j++;
                temp=0;
            }
            k++;            
        }
        while(i<nL){

            mergeArr[k]=left[i]; k++; i++;
        }
        while(j<nR){

            mergeArr[k]=right[j]; k++; j++;
        }


        return new String(mergeArr);

    }
}
