/**
 * 
 */
package coreJava;

/**
 * @author malay
 *java program two nonnegative integers a and b are given. 
 *integer A occurs in integer B at position p if the decimal representation of A is substring starting position p(count from zero)
 * of the decimal representation of B. 
 * Decimal representation are assumed to be big-endian and without lending zeros (only exception being the number 0,whose decimal representation is "0" )
 *
 */
public class FindPositionOfSubString {
	
	static int countSubString(long a, long b) {
		
		int i = (int) b;
		int j = (int) a;
		
		String str1 = String.valueOf(i);
		String str2 = String.valueOf(j);
		int str= str1.indexOf(str2);
		
		return str;
	}

	public static void main(String[] args) {
		
		System.out.println(countSubString(73, 12735356));
	}
	
}
