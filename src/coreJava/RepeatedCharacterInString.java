/**
 * 
 */
package coreJava;

import java.util.HashMap;
import java.util.Set;

/**
 * @author malay
 *
 */
public class RepeatedCharacterInString {

	public static void findDuplicateChars(String inputString)
	{
		HashMap<Character,Integer> hashMap=new HashMap<Character,Integer>();
		
		char[] charArray=inputString.toLowerCase().toCharArray();
		
		if(charArray.length==0)
		{
			System.out.println("String is null");
		}
		else
		{
			for(char ch:charArray)
			{
				if(hashMap.containsKey(ch))
				{
					hashMap.put(ch,hashMap.get(ch)+1);
				}
				else
				{
					hashMap.put(ch,1);
				}
			}
		}
		System.out.println(hashMap);
		Set<Character> set=hashMap.keySet();
		
		for(Character ch:set)
		{
				if(hashMap.get(ch)>1)
				{
					System.out.println(ch+":"+hashMap.get(ch));
				}
		}
	}
	public static void main(String args[])
	{
		findDuplicateChars("JavaJ2ee");
	}
}
