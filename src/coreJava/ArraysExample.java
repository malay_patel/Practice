package coreJava;

import java.util.Arrays;


public class ArraysExample {

	
	public static void main(String[] args) {
		int[] numberArray = new int[19];
		int[] numberArray2 = {5,5,8,2,1,3,8,9,5};
		Arrays.fill(numberArray, 10);
		Arrays.fill(numberArray, 4, 10, 8);
		System.out.println(Arrays.equals(numberArray, numberArray2));
		for(int i : numberArray) {
			System.out.println(i+" ");
		}
	}
}
