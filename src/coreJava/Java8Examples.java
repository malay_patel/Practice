/**
 * 
 */
package coreJava;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author malay
 *
 */
public class Java8Examples {

	public static void main(String[] args) {
		List<String> lines = Arrays.asList("Malay", "Patel", "Devanshi");
		List<String> result = lines.stream().filter(line->!"Devanshi".equals(line)).collect(Collectors.toList());
		result.forEach(System.out::println);
		
		List<Person> persons = Arrays.asList(
				new Person("Malay", 24),
				new Person("Patel", 25),
				new Person("Devanshi", 24)
				);
		Person result1 = persons.stream().filter(p->"Malay".equals(p.getName())).findAny().orElse(null);
		System.out.println(result1.getName());
		
	}
}
