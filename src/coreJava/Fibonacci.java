/**
 * 
 */
package coreJava;

import java.util.ArrayList;
import java.util.List;

/**
 * @author malay
 *
 */
public class Fibonacci {
		 
	
	public int fib(int number) {
		/*if(number<1)
			return 0;
		else*/ if(number<2) 
			return 1;
		return fib(number-1) + fib(number-2);
	}
	
	    public static void main(String a[]){
	         
	         int febCount = 20;
	         int[] feb = new int[febCount];
	         feb[0] = 0;
	         feb[1] = 1;
	         for(int i=2; i < febCount; i++){
	             feb[i] = feb[i-1] + feb[i-2];
	         }
	 
//	         for(int i=0; i< febCount; i++){
//	                 System.out.print(feb[i] + " ");
//	         }
	         for (int i : feb) {
				System.out.print(i+" ");
			}
	         
	         Fibonacci fb = new Fibonacci();
	        /* int[] feb = new int[febCount];
	         int i1=fb.fib(febCount);
	         //feb[0] = 0;
	         for(int i=0; i<febCount; i++) {
	        	 feb[i] = fb.fib(i);
	        	 System.out.print(feb[i]+ " ");
	         }
	         for(int i=0; i<febCount; i++) {
	        	 System.out.print(feb[i]+" ");
	         }
	        	 */
	         
	         
	         
	         
	         System.out.println(fb.fib(4));
	    }
	
}
