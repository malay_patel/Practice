package coreJava;

@Deprecated
public class WalkingRobot {
	static int U = 0;
	static int R = 1;
	static int D = 2;
	static int L = 3;

	public static void main(String[] args) {
		String path = "RRRULD";
		isWalking(path);
	}

	private static void isWalking(String path) {
		char[] pathChar = path.toCharArray();
		
		int x=0, y=0;
		int dir = U;
		
		for(int i=0; i<pathChar.length; i++) {
			char move = pathChar[i];
			if(move == U) y++;
			else if(move == R) x++;
			else if(move == D) y--;
			else x--;
			
			/*char move = pathChar[i];
			if(move == 'R') {
				dir = (dir+1)%4;
			}
			else if(move == 'L') {
				dir = (4+dir-1)%4;
			}
			else {
				if(dir == U) y++;
				else if(dir == R) x++;
				else if(dir == D) y--;
				else x--;
			}*/
		}
		System.out.println("x: "+x +" y: "+y);
	}
}
