/**
 * 
 */
package coreJava;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * @author malay
 *
 */
public class ObjectSerialization {

	public static void saveObject(Serializable object, String path) throws IOException {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
			oos.writeObject(object);
		}
	}

	public static Object loadObject(String path) throws ClassNotFoundException, IOException {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
			return ois.readObject();
		}
	}

	public static void main(String[] args) {
		ObjectToSerialize object = new ObjectToSerialize("Malay", "Patel");
		try {
			String path = "/Users/malay/git/Practice/src/coreJava/object.bin";
			saveObject(object, path);
			System.out.println("Object serialized: " + object);

			ObjectToSerialize deserializedObject = (ObjectToSerialize) loadObject(path);
			System.out.println("Object deserialized: " + deserializedObject);
			System.out.println("They are equals: " + object.equals(deserializedObject));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
