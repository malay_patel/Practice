/**
 * 
 */
package geekforgeeks;

import java.util.Scanner;

/**
 * @author malay
 *The task is to count all the possible paths from top left to bottom right of a mXn matrix with the constraints that from each cell you can either move only to right or down.

Input: 
First line consists of T test cases. First line of every test case consists of N and M, denoting the number of rows and number of column respectively.

Output: 
Single line output i.e count of all the possible paths from top left to bottom right of a mXn matrix. Since output can be very large number use %10^9+7.

Constraints:
1<=T<=100
1<=N<=100
1<=M<=100

***
***
***
Example:
Input:
1
3 3
Output:
6
 */
public class CountAllPossiblePathsFromTopLeftToBottomRight {

	  static final int mod=1000000007;
	    static int solve(int r, int c){
	        int res=0;
	        int dp[][]= new int[r][c];
	        for(int i=0; i<c; i++)
	            dp[0][i]=1;
	        for(int i=0; i<r; i++)
	            dp[i][0]=1;
	        for(int i=1; i<r; i++){
	            for(int j=1; j<c; j++){
	                dp[i][j]=(dp[i-1][j]%mod+dp[i][j-1]%mod)%mod;
	            }
	        }
	        
	        return dp[r-1][c-1];
	    }
		public static void main (String[] args) {
			//code
			Scanner sc= new Scanner(System.in);
			int t= sc.nextInt();
			while(t-- >0){
			    int r= sc.nextInt();
			    int c= sc.nextInt();
			    System.out.println(solve(r,c));
			}
		}
}
