package geekforgeeks;

import java.util.Scanner;

/**
 * @author malay
 *Stickler is a thief and wants to loot money from a society of n houses placed in a line. He is a weird person and follows a rule while looting the houses and according to the rule he will never loot two consecutive houses. At the same time, he wants to maximize the amount he loots. The thief knows which house has what amount of money but is unable to find the maximum amount he can end up with. He asks for your help to find the maximum money he can get if he strictly follows the rule. Each house has a[i] amount of money present in it.

Input:
The first line of input contains an integer T denoting the number of test cases. Each test case contains an integer n which denotes the number of elements in the array a[]. Next line contains space separated n elements in the array a[].

Output:
Print an integer which denotes the maximum amount he can take home.

Constraints:
1<=T<=200
1<=n<=1000
1<=a[i]<=10000

Example:
Input:
2
6
5 5 10 100 10 5
3
1 2 3

Output:
110
4
 *
 */
public class SticklerThief {
	public static void main (String[] args) {
		//code
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		for(int i=0;i<t;i++)
		{
		    int  n = sc.nextInt();
		    int arr[] = new int[n];
		    for(int j=0;j<n;j++)
		    arr[j] = sc.nextInt();
		    System.out.println(maxSum(n,arr));
		}
	}
	
	public static int maxSum(int n ,int arr[])
	{  if (n==1)
	    return arr[0];
	    int dp[] = new int[n];
	    dp[0] =arr[0];
	    dp[1]= Math.max(arr[0],arr[1]);
	    for(int i=2;i<n;i++)
	     dp[i] = Math.max(dp[i-2]+arr[i],dp[i-1]);
	     return dp[n-1];
	}
}
