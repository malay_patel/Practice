/**
 * 
 */
package java8;

import java.util.UUID;

/**
 * @author malay
 *
 */
public class GenerateUUID {

	public static void main(String[] args) {
		final String uuid = UUID.randomUUID().toString().replace("-", "");
		final String uuid1 = UUID.randomUUID().toString();
		
		System.out.println("uuid: "+uuid);
		System.out.println("uuid1: "+uuid1);
		System.out.println(uuid.length()+" "+uuid1.length());
	}
}
