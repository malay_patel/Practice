package java8;

import java.time.LocalTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.sun.corba.se.impl.orbutil.closure.Future;

class MyResourceService extends Thread {

	static int input1 = 100;
	// Really resource consuming resource being accessed by multiple clients
	public int produce(int input) {
		// Assuming this is really resource consuming
		System.out.println("in real produce method "+input);
		return input + 100;
	}

	// MAIN emthod you want to MODIFY
	// Write an async version of the same with public access
	// You want to allow multiple clients to be able to access the system produce
	// function in parallel
	public int asyncProduce(int input) {

		Runnable task = new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("In Runnable " + produce(input));
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};

		new Thread(task, "ServiceThread").start();
		return produce(input);
	}
	
	Executor executor = Executors.newFixedThreadPool(10);
	CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
	    try {
	    		
	    		System.out.println("In Executor " + produce(input1));
	        TimeUnit.SECONDS.sleep(1);
	    } catch (InterruptedException e) {
	        throw new IllegalStateException(e);
	    }
	    return String.valueOf(produce(input1));
	}, executor);

	// Feel free to define your helper classes here!!!

	public static void main(String[] args) throws java.lang.Exception {
		System.out.println("Hello Costly Resource Service!!!!");

		MyResourceService s = new MyResourceService();
		// Client: sync
		// s.produce(100);

		System.out.println(LocalTime.now());
		// s.asyncProduce(100);
		// System.out.println("async"+s.asyncProduce(100));
		//s.produce(100);
		System.out.println("async "+s.future.get());
		System.out.println(LocalTime.now());

		// Test code goes here :::

		// Client: async
		// :
		// s.asyncProduce(...)
		// :
		// :

	}
}
