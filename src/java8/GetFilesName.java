/**
 * 
 */
package java8;

import java.io.File;
import java.util.ArrayList;

/**
 * @author malay
 *
 */
public class GetFilesName {

	public static void main(String[] args) {

		ArrayList<File> listFiles = new ArrayList<File>();
		getFiles("/Users/malay/Documents/Scanned File/", listFiles);
	}
	
	public static File[] getFiles(String file, ArrayList<File> listFiles) {

		File files = new File(file);
		File[] listOfFiles = files.listFiles();
		{
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					System.out.println("File " + listOfFiles[i].getName());
					listFiles.add(listOfFiles[i]);
				} else if (listOfFiles[i].isDirectory()) {
					System.out.println("Directory " + listOfFiles[i].getName());
					getFiles(listOfFiles[i].getAbsolutePath(), listFiles);
				}
			}
		}
		return listOfFiles;
	}
}