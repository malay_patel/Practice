package java8;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class PartitionToNFilesFromOneFile {
	public static void main(String[] args) throws IOException {

		RandomAccessFile raf = new RandomAccessFile("/Users/malaypatel/Desktop/Test/test.mpg", "r");

		long numPartition = 10;
		long fileSize = raf.length();
		long sizePerSplit = fileSize / numPartition;
		long remainingSize = fileSize % numPartition;

		int maximumBufferSize = 8 * 100000;
		for (int k = 1; k <= numPartition; k++) { // For each partition
			BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream("/Users/malaypatel/Desktop/Test/split" + k+".mpg"));
			if (sizePerSplit > maximumBufferSize) {
				long numReads = sizePerSplit / maximumBufferSize;
				long numRemainingRead = sizePerSplit % maximumBufferSize;
				for (int i = 0; i < numReads; i++) { // Number of reads.
					writeFile(raf, bw, maximumBufferSize);
				}
				if (numRemainingRead > 0) {
					writeFile(raf, bw, numRemainingRead);
				}
			} else {
				writeFile(raf, bw, sizePerSplit);
			}
			bw.close();
		}
		if (remainingSize > 0) {
			BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream("/Users/malaypatel/Desktop/Test/split" + (numPartition + 1)+".mpg"));
			writeFile(raf, bw, remainingSize);
			bw.close();
		}
		raf.close();
	}

	static void writeFile(RandomAccessFile raf, BufferedOutputStream bw, long numBytes) throws IOException {
		byte[] buf = new byte[(int) numBytes];
		int val = raf.read(buf);
		if (val != -1) {
			bw.write(buf);
		}

	}
}
