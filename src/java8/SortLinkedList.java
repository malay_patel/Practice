/**
 * 
 */
package java8;

import java.text.Collator;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author malay
 *
 */
public class SortLinkedList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> sortLinkedList = new LinkedList<Integer>();
		sortLinkedList.add(1);
		sortLinkedList.add(3);
		sortLinkedList.add(5);
		sortLinkedList.add(2);
		sortLinkedList.add(4);
		sortLinkedList.add(6);
		sortLinkedList.add(8);
		sortLinkedList.remove(2);
		
		System.out.println(sortLinkedList);
		Collections.sort(sortLinkedList);
		System.out.println(sortLinkedList);

		sortLinkedList.sort(Comparator.reverseOrder());
		System.out.println(sortLinkedList);
		
		
		LinkedList<String> stringLinkedList = new LinkedList<String>(Arrays.asList("Malay", "Devanshi", "malay", "devanshi"));
		
		System.out.println(stringLinkedList);
		
		stringLinkedList.sort(Comparator.naturalOrder());
		System.out.println(stringLinkedList);
		
		/*stringLinkedList.sort(new Comparator<String>(){

			@Override
			public int compare(String o1, String o2) {
				return Collator.getInstance().compare(o1, o2);
			}
			
		});*/
		
		Collections.sort(stringLinkedList, (o1,o2)->Collator.getInstance().compare(o1, o2));
		System.out.println(stringLinkedList);
	}

}
