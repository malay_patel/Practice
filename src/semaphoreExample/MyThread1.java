/**
 * 
 */
package semaphoreExample;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;
import java.util.concurrent.Semaphore;

/**
 * @author malay
 *
 */
class Shared1 {
	static int count = 0;
}

public class MyThread1 extends Thread {

	Semaphore sema;
	String threadNames;

	public MyThread1(Semaphore sema, String threadNames) {
		super(threadNames);
		this.sema = sema;
		this.threadNames = threadNames;
	}

	@Override
	public void run() {

		if (this.getName().equals("Malay")) {
			
			String uuid = UUID.randomUUID().toString().replace("-", "");

			System.out.println("Starting Thread- Name: " + this.getName() +" UUID: "+ uuid+ " Time: "+ LocalDateTime.now()+ " Thread State: " +this.getState());

			try {

				System.out.println(this.getName() + uuid+ " is waiting for a permit");

				sema.acquire();

				System.out.println(this.getName() +uuid+ " gets a permit");

				for (int i = 0; i < 5; i++) {
					Shared1.count++;
					System.out.println(this.getName() +uuid+ " : " + Shared1.count);

					Thread.sleep(100);
				}

				sema.release();

				System.out.println(this.getName() +uuid+" releases the permit");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		else if (this.getName().equals("Devanshi")){
			System.out.println("Starting Thread: " + this.getName());

			try {

				System.out.println(this.getName() + " is waiting for a permit");

				sema.acquire();

				System.out.println(this.getName() + " gets a permit");

				for (int i = 0; i < 5; i++) {
					Shared1.count--;
					System.out.println(this.getName() + " : " + Shared1.count);

					Thread.sleep(100);
				}

				sema.release();

				System.out.println(this.getName() + " releases the permit");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		else if (this.getName().equals("Malay1")){
			System.out.println("Starting Thread: " + this.getName());

			try {

				System.out.println(this.getName() + " is waiting for a permit");

				sema.acquire();

				System.out.println(this.getName() + " gets a permit");

				for (int i = 0; i < 5; i++) {
					Shared1.count++;
					System.out.println(this.getName() + " : " + Shared1.count);

					Thread.sleep(100);
				}

				sema.release();

				System.out.println(this.getName() + " releases the permit");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		else if (this.getName().equals("Devanshi1")){
			System.out.println("Starting Thread: " + this.getName());

			try {

				System.out.println(this.getName() + " is waiting for a permit");

				sema.acquire();

				System.out.println(this.getName() + " gets a permit");

				for (int i = 0; i < 5; i++) {
					Shared1.count--;
					System.out.println(this.getName() + " : " + Shared1.count);

					Thread.sleep(100);
				}

				sema.release();

				System.out.println(this.getName() + " releases the permit");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
