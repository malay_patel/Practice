/**
 * 
 */
package semaphoreExample;

import java.util.UUID;
import java.util.concurrent.Semaphore;

/**
 * @author malay
 *
 */
public class SemaphoreDemo1 {

	public static void main(String[] args) throws InterruptedException {

		Semaphore semaphore = new Semaphore(1, true);

		MyThread1 t1 = new MyThread1(semaphore, "Malay");
		MyThread1 t2 = new MyThread1(semaphore, "Devanshi");
		MyThread1 t3 = new MyThread1(semaphore, "Malay1");
		MyThread1 t4 = new MyThread1(semaphore, "Devanshi1");
		

		t1.start();
		t2.start();
		t3.start();
		t4.start();

		t1.join();
		t2.join();
		t3.join();
		t4.join();

		System.out.println("Count: " + Shared1.count);

	}

}
